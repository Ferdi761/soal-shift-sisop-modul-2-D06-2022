#include <stdio.h>
#include <pwd.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

int status;
char hewan[100][100], hewanDarat[100][100], hewanAir[100][100], burung[100][100];
int burungSize = 0, countAir = 0;

void createDirectory(char path[]);
void unzip(char destination[], char target[]);
void listAnimal(char path[]);
void moveAnimalDaratOrAir(char path[], int index, char *type);
void removeDirectory(char path[]);
void removeFile(char path[]);
void removeBirdFile();
void addList(int index);

int main(int argc, char const *argv[])
{
  char pathDarat[] = "/home/elelel/modul2/darat/";
  char pathAir[] = "/home/elelel/modul2/air/";
  char pathAnimal[] = "/home/elelel/modul2/animal/";
  char path[] = "/home/elelel/modul2/";
  char targetZIP[] = "animal.zip";

  createDirectory(pathDarat);
  sleep(3);
  createDirectory(pathAir);

  unzip(path, targetZIP);
  listAnimal(pathAnimal);
  removeDirectory(pathAnimal);
  removeBirdFile();

  for (int i = 0; i < countAir; i++)
    addList(i);

  return 0;
}

void createDirectory(char path[])
{
  if (fork() == 0)
    execlp("mkdir", "mkdir", "-p", path, NULL);
  while ((wait(&status)) > 0)
    ;
}

void unzip(char destination[], char target[])
{
  if (fork() == 0)
    execlp("unzip", "unzip", "-d", destination, "-q", target, NULL);

  while ((wait(&status)) > 0)
    ;
}

void listAnimal(char path[])
{
  DIR *d;
  struct dirent *dir;
  int count = 0, countDarat = 0;
  d = opendir(path);

  if (d)
  {
    while ((dir = readdir(d)) != NULL)
    {
      if ((strstr(dir->d_name, "..") == 0) && (strcmp(dir->d_name, "") != 0) && (strcmp(dir->d_name, ".") != 0) && (strstr(dir->d_name, "txt") == 0))
      {
        strcpy(hewan[count], dir->d_name);
        count++;
      }
    }
    closedir(d);
  }

  for (int i = 0; i < count; i++)
  {
    char copyPath[100];
    strcpy(copyPath, path);

    if (strstr(hewan[i], "bird"))
    {
      strcpy(burung[burungSize], hewan[i]);
      burungSize++;
    }
    if (strstr(hewan[i], "darat"))
    {
      strcpy(hewanDarat[countDarat], hewan[i]);
      moveAnimalDaratOrAir(copyPath, i, "darat");
      countDarat++;
    }
    else if (strstr(hewan[i], "air"))
    {
      strcpy(hewanAir[countAir], hewan[i]);
      moveAnimalDaratOrAir(copyPath, i, "air");
      countAir++;
    }
  }
}

void moveAnimalDaratOrAir(char path[], int index, char *type)
{
  strcat(path, hewan[index]);
  char dest[100];
  if (strcmp(type, "air") == 0)
    strcpy(dest, "/home/elelel/modul2/air");
  else
    strcpy(dest, "/home/elelel/modul2/darat");
  if (fork() == 0)
  {
    execlp("mv", "mv", path, dest, NULL);
  }
  while ((wait(&status)) > 0)
    ;
}

void removeDirectory(char path[])
{
  int status;
  if (fork() == 0)
    execlp("rm", "rm", "-r", path, NULL);
  while ((wait(&status)) > 0)
    ;
}

void removeFile(char path[])
{
  int status;
  if (fork() == 0)
    execlp("rm", "rm", "--force", path, NULL);
  while ((wait(&status)) > 0)
    ;
}

void removeBirdFile()
{
  for (int i = 0; i < burungSize; i++)
  {
    char source[100] = "/home/elelel/modul2/darat/";
    strcat(source, burung[i]);
    removeFile(source);
  }
}

void addList(int index)
{
  char permission[5];
  struct stat fs;
  int r;
  char path[] = "/home/elelel/modul2/air/";
  r = stat(path, &fs);
  if (r == -1)
  {
    fprintf(stderr, "File error\n");
    exit(1);
  }

  struct passwd *pw = getpwuid(fs.st_uid);
  if (fs.st_mode & S_IRUSR)
    permission[0] = 'r';
  if (fs.st_mode & S_IWUSR)
    permission[1] = 'w';
  if (fs.st_mode & S_IXUSR)
    permission[2] = 'x';

  if (pw != 0)
  {
    FILE *wfile = fopen("/home/elelel/modul2/air/list.txt", "a");
    fprintf(wfile, "%s_%s_%s\n", pw->pw_name, permission, hewanAir[index]);
    fclose(wfile);
  }
}
