#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <wait.h>
#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <json-c/json.h>

void run_exec(char path[], char *argv[]) {
	pid_t testid;
	int status;

	testid = fork();

	if(testid < 0) exit(EXIT_FAILURE);

	if(testid == 0) {
		execv(path, argv);
		exit(0);
	}

	else while((wait(&status)) > 0);
}


int main() {

	//making pid and session id
	pid_t proid, sid, child_sid, parent_sid, zipid, downid;

	//forking, "child_id" as a parent will call another process (child process)
	proid = fork();

	if(proid < 0) {
		//stop the program if fail to create new process
		exit(EXIT_FAILURE);
	}

	//parent process
	if(proid > 0) {
		//killing parent process
		exit(EXIT_SUCCESS);
	}

	//get advantages of file permission
	umask(0);

	//making unique session ID for child process
	sid = setsid();
	
	if(sid < 0) {
		exit(EXIT_FAILURE);
	}

	//set root as working directory
	if((chdir("/home/achferdi/Documents/sisop")) < 0) {
		exit(EXIT_FAILURE);
	}


	//closing all standart file descriptor
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	//init primogems
	int gems = 79000;
	

	//loop our main program in here
	
	while(1){

		int status;

		char *weapondown[] = {
					"wget",
					"https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT",
					"-O",
					"db_weapons",
					"-q",
					NULL};
		run_exec("/usr/bin/wget", weapondown);

		char *chardown[] = {
					"wget",
					"https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp",
					"-O",
					"db_character",
					"-q",
					NULL};
		run_exec("/usr/bin/wget", chardown);

		char *makedir[] = {"mkdir", "-p", "gacha_gacha", NULL};
		run_exec("/usr/bin/mkdir", makedir);

		sleep(30);
		
	} 
	return 0;

}