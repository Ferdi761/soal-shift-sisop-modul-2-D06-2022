# Praktikum 2 sisop

## ✍️ Authors

- Achmad Ferdiansyah 5025201245
- Nazhifah Elqolby 5025201156
- Ravin Pradhitya 5025201068

# Soal 1

## Catatan

- Menggunakan fork dan exec.
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename().
- Tidak boleh pake cron.
- Semua poin dijalankan oleh 1 script di latar belakang. Cukup jalankan script 1x serta ubah time dan date untuk check hasilnya.

Membuat sistem gacha pada C

## A. Membuat fungsi dengan 2 parameter untuk menangani exec

```
void run_exec(char path[], char *argv[]) {
	pid_t testid;
	int status;

	testid = fork();

	if(testid < 0) exit(EXIT_FAILURE);

	if(testid == 0) {
		execv(path, argv);
		exit(0);
	}

	else while((wait(&status)) > 0);
}
```

## B. Masuk ke main dan membuat daemon (inisiasi dan forking)

```
pid_t proid, sid, child_sid, parent_sid, zipid, downid;

	//forking, "child_id" as a parent will call another process (child process)
	proid = fork();

	if(proid < 0) {
		//stop the program if fail to create new process
		exit(EXIT_FAILURE);
	}
```

## C. Kill parrent process

```
  //parent process
	if(proid > 0) {
		//killing parent process
		exit(EXIT_SUCCESS);
	}
```

## D. Mendapatkan akses file

```
  //get advantages of file permission
	umask(0);
```

## E. Set unique session ID pada child process agar tidak terjadi orphan process

```
  //making unique session ID for child process
	sid = setsid();

  if(sid < 0) {
		exit(EXIT_FAILURE);
	}
```

## F. Set working directory ke root

```
  //set root as working directory
	if((chdir("/")) < 0) {
		exit(EXIT_FAILURE);
	}
```

## G. Menutup semua file descriptor aar menghindari interaksi user

```
  //closing all standart file descriptor
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
```

## H. Init Primogems

```
	//init primogems
	int gems = 79000;
```

## I. Masuk ke loop dan download file character dan weapon

```
while(1){

		int status;

		char *weapondown[] = {
					"wget",
					"https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT",
					"-O",
					"db_weapons",
					"-q",
					NULL};
		run_exec("/usr/bin/wget", weapondown);

		char *chardown[] = {
					"wget",
					"https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp",
					"-O",
					"db_character",
					"-q",
					NULL};
		run_exec("/usr/bin/wget", chardown);
```

## J. Membuat directory gacha_gacha

```
char *makedir[] = {"mkdir", "-p", "gacha_gacha", NULL};
		run_exec("/usr/bin/mkdir", makedir);
```

## K. instirahat selama 30 detik dan return 0

```
sleep(30);

	}
	return 0;

}
```

# Soal 2

## Catatan

- File zip berada dalam drive modul shift ini bernama drakor.zip
- File yang penting hanyalah berbentuk .png
- Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun
  rilis]:[kategori]. Jika terdapat lebih dari satu drama dalam poster, dipisahkan
  menggunakan underscore(\_).
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di
  bahasa C.
- Gunakan bahasa pemrograman C (Tidak boleh yang lain).
- Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).
- [user] menyesuaikan nama user linux di os anda.

## A. Mengextract zip dan hapus folder yang tidak dibutuhkan

```
int main() {
	BackToHome();
	ExtractFiles();
	ChangeWorkingDirectory("shift2/drakor");
	DeleteFolders();
    CreateCategory();
	ProcessCategoryData();

  	return 0;
}
```

- Pada saat unzip hanya file yang berbentuk .png yang akan ke folder tujuan. Tidak semua file dan folder akan terunzip dengan sendirinya.

```
void DeleteFolders() {
	struct dirent *dp;
	DIR *dir = opendir("./");

	if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 	{
        	struct stat fs;

		    int r = stat(dp->d_name,&fs);
    		if( r==-1 ) {
        		fprintf(stderr,"File error\n");
        		exit(1);
    		}

    		//Delete non-PNG
    		if(fs.st_mode != 0100664) {
    			RemoveFile(dp->d_name);
    		}
       	}
   	}
    closedir(dir);
}
```

- Untuk mengecek file dan folder yang ada di dalam folder drakor.
- Hapus folder jika tipe filenya bukan PNG.

## B. Membuat folder untuk setiap jenis drakor

```
void CreateCategory() {
	struct dirent *dp;
	DIR *dir = opendir("./");

	if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 	{
        	ProcessPhoto(dp->d_name);
        }
    }
    closedir(dir);
}
```

- Untuk mengecek file yang ada di dalam folder drakor.

```
void ProcessPhoto(char name[]) {
	int i = 0;
	while(name[i] != '.') {
		char title[35], year[10], category[20];

		ParseString(title, name, &i);
		i++;

		ParseString(year, name, &i);
		i++;

		ParseString(category, name, &i);
		if(name[i] == '_') {
			i++;
		}
		CreateFolder(category);
```

- Untuk memisahkan nama file menjadi title, year, dan category-nya.
- Pemisahan nama file akan dijalankan lagi jika diketahui di dalam foto tersebut terdapat dua poster.

## C dan D. Memindahkan poster ke folder sesuai kategori dan di rename dengan nama & foto harus di pindah ke masing-masing kategori

```
        CopyFile(name, category);
		ChangeWorkingDirectory(category);
		RenameFile(name, title);
		InsertData(year, title);
		ChangeWorkingDirectory("../");
	}
	RemoveFile(name);
}
```

- Melakukan perulangan dan lanjutan dari soal 2B.

```
void InsertData(char year[], char title[]) {
	FILE *file = fopen("temp.txt" , "a");

	fprintf(file, "%s %s\n", year, title);

	fclose(file);
}
```

- Untuk menyimpan informasi berupa judul film dan tahun rilis dari beberapa film tersebut.

## E. Membuat file data.txt yang berisi nama dan tahun rilis semua drakor

```
void ProcessCategoryData() {
	struct dirent *dp;
	DIR *dir = opendir("./");

	if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL) {
       	if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) 	{
        	ChangeWorkingDirectory(dp->d_name);
        	SortData();
            ChangeWorkingDirectory("../");
        }
    }
   	closedir(dir);
}
```

- Jika dilihat dalam penyelesaian poin c. dan d., dilakukan print nama file dan juga tahun di data.txt dengan folder sesuai genre dari masing-masing drakor.

```
void SortData() {
    	FILE *file = fopen("sort.txt" , "w");
    	fclose(file);

    	char *argv[] = {"sort", "-o", "sort.txt", "temp.txt", NULL};
    	Execute(argv, "/usr/bin/sort");

    	RemoveFile("temp.txt");
}
```

- Untuk menyortir file tempt.txt karena filenya berisi tahun rilis serta nama filmnya.

# Soal 3

## Catatan

- Tidak boleh memakai system().
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec dan fork
- Direktori “.” dan “..” tidak termasuk

## A. Membuat Folder modul2

Untuk dapat membuat folder dengan menggunakan bahasa c digunakanlah execlp untuk menjalankan argumen sebagai command lalu digunakan -p agar jika parent folder belom dibuat maka parent folder akan dibuat juga. Lalu digunakan "while(wait(&status)) > 0" agar tidak exit dari function sebelum tugas dalam function selesai dikerjakan.

```
 void createDirectory(char path[])
{
  if (fork() == 0)
    execlp("mkdir", "mkdir", "-p", path, NULL);
  while ((wait(&status)) > 0)
    ;
}
```

### 1. Membuat Folder darat

Digunakanlah function createDirectory yang telah dijelaskan diatas dengan memasukkan path untuk folder darat pada main.

```
char pathDarat[] = "/home/elelel/modul2/darat/";
createDirectory(pathDarat);
```

### 2. Setelah 3s Membuat Folder air

Digunakanlah function createDirectory yang telah dijelaskan diatas dengan memasukkan path untuk folder air pada main.

```
char pathAir[] = "/home/elelel/modul2/air/";
sleep(3);
createDirectory(pathAir);
```

## B. Melakukan Unzip File animal.zip

Sama untuk menjalankan command unzip pada script c digunakanlah execlp diisi dengan command yang ingin dijalankan yaitu unzip dan digunakan -d untuk membuat destinasi untuk hasil dari unzip dan digunakan -q ("quiet") agar tidak menghasilkan verbose pada terminal. Digunakkan while(wait(&status)) untuk dapat menghindari exit function sebelum script selesai dijalankan.

```
void unzip(char destination[], char target[])
{
  if (fork() == 0)
    execlp("unzip", "unzip", "-d", destination, "-q", target, NULL);

  while ((wait(&status)) > 0)
    ;
}
```

Digunakanlah function unzip yang telah dijelaskan diatas dengan mengisi path untuk target dan destination pada main.

```
char path[] = "/home/elelel/modul2/";
char targetZIP[] = "animal.zip";
unzip(path, targetZIP);
```

## C. Memindahkan file dari animal.zip

Pertama-tama kita akan membuka folder yang diinginkan yaitu folder animal lalu melakukan iterasi selama tidak NULL atau selama cursor file masih ada. Lalu setiap iterasi file pada folder dilakukan pengcopyan nama file (dir->d_name) ke array global hewan serta menambahkan sebanyak satu pada variabel count untuk menghitung berapa banyak jumlah file yang ada pada folder yang dibuka. Lalu jika sudah selesai melakukan iterasi pada folder, folder akan ditutup. Lalu dilakukanlah penglistan hewan sesuai kategori yang ada yaitu "air", "darat", dan "burung" dengan melakukan iterasi sebanyak jumlah hewan yang ada. Jika nama hewan mengandung kata "burung" (dapat digunakan fungsi string strstr) maka nama hewan tersebut akan dicopy ke array burung dan variabel burungSize akan ditambah sebanyak 1 (banyaknya jumlah file yang mengandung kata burung). Lalu jika hewan juga mengandung kata "darat" maka nama hewan tersebut juga akan dicopy ke array hewanDarat dan variabel countDarat akan ditambah sebanyak 1 (banyaknya jumlah file mengandung kata darat) dan akan melakukan pemanggilan function moveAnimalDaratOrAir dengan parameter type adalah "darat". Namun jika nama hewan mengandung kata "air" maka nama hewan tersebut akan dicopy ke array hewanAir dan variabel countAir akan ditambah sebanyak 1 dan akan melakukan pemanggilan function moveAnimalDaratOrAir dengan parameter type adalah "air".

```
void listAnimal(char path[])
{
  DIR *d;
  struct dirent *dir;
  int count = 0, countDarat = 0;
  d = opendir(path);

  if (d)
  {
    while ((dir = readdir(d)) != NULL)
    {
      if ((strstr(dir->d_name, "..") == 0) && (strcmp(dir->d_name, "") != 0) && (strcmp(dir->d_name, ".") != 0) && (strstr(dir->d_name, "txt") == 0))
      {
        strcpy(hewan[count], dir->d_name);
        count++;
      }
    }
    closedir(d);
  }

  for (int i = 0; i < count; i++)
  {
    char copyPath[100];
    strcpy(copyPath, path);

    if (strstr(hewan[i], "bird"))
    {
      strcpy(burung[burungSize], hewan[i]);
      burungSize++;
    }
    if (strstr(hewan[i], "darat"))
    {
      strcpy(hewanDarat[countDarat], hewan[i]);
      moveAnimalDaratOrAir(copyPath, i, "darat");
      countDarat++;
    }
    else if (strstr(hewan[i], "air"))
    {
      strcpy(hewanAir[countAir], hewan[i]);
      moveAnimalDaratOrAir(copyPath, i, "air");
      countAir++;
    }
  }
}
```

Untuk memindahkan dapat digunakan dengan "execlp("mv", "mv", path, dest, NULL)" dimana path adalah path source dari file dan dest adalah path source setelah dipindahkan nantinya. Untuk menentukan pembagiannya sesuai kategori digunakanlah strcmp antara type dan "air" dimana jika menghasilkan 0 maka artinya type dan "air" sama dan merupakan kategori air maka melakukan pengcopyan path air ke variabel dest, begitupun sebaliknya. Digunakkan while(wait(&status)) untuk dapat menghindari exit function sebelum script selesai dijalankan.

```
void moveAnimalDaratOrAir(char path[], int index, char *type)
{
  strcat(path, hewan[index]);
  char dest[100];
  if (strcmp(type, "air") == 0)
    strcpy(dest, "/home/elelel/modul2/air");
  else
    strcpy(dest, "/home/elelel/modul2/darat");
  if (fork() == 0)
  {
    execlp("mv", "mv", path, dest, NULL);
  }
  while ((wait(&status)) > 0)
    ;
}
```

Dipanggilah function listAnimal dengan path animal untuk memindahkan file ke folder kategorinya masing-masing dan melakukan penglistan nama file hewan ke global array yang telah disediakan sesuai dengan kategori air dan darat pada fungsi main.

```
char pathAnimal[] = "/home/elelel/modul2/animal/";
listAnimal(pathAnimal);
```

### 1. Menghapus file yang tidak mengandung baik "air" maupun "darat"

Untuk dapat menghapus file serta folder yang tidak dibutuhkan yaitu animal dan file yang tidak sesuai dengan format dapat dilakukan langsung dengan rm -r ("recursive") path. Lalu digunakan while wait untuk dapat menghindari exit function sebelum script selesai dijalankan.

```
void removeDirectory(char path[])
{
  int status;
  if (fork() == 0)
    execlp("rm", "rm", "-r", path, NULL);
  while ((wait(&status)) > 0)
    ;
}
```

Karena kita ingin menghapus file yang berada di dalam folder animal serta folder animal, maka kita bisa langsung saja menghapus satu direktori dengan menggunakan function removeDirectory dan memparse path untuk animal pada main.

```
char pathAnimal[] = "/home/elelel/modul2/animal/";
removeDirectory(pathAnimal);
```

## D. Menghapus file yang mengandung "burung" dari folder darat

Untuk dapat menghapus file yang diinginkan dapat dilakukan langsung dengan rm --force path. Lalu digunakan while wait untuk dapat menghindari exit function sebelum script selesai dijalankan.

```
void removeFile(char path[])
{
  int status;
  if (fork() == 0)
    execlp("rm", "rm", "--force", path, NULL);
  while ((wait(&status)) > 0)
    ;
}
```

Melakukan concanate string path darat dengan nama file burung lalu melakukan penginterasian sebanyak jumlah file yang mengandung kata "burung" pada file darat.

```
void removeBirdFile()
{
  for (int i = 0; i < burungSize; i++)
  {
    char source[100] = "/home/elelel/modul2/darat/";
    strcat(source, burung[i]);
    removeFile(source);
  }
}
```

Memanggil function removeBirdFile yang telah dijelaskan diatas pada main

```
removeBirdFile();
```

## E. Melakukan list untuk nama file dari folder air ke list.txt

Untuk format nama list yang digunakkan adalah UID_UID Permission(rwx)\_namafile.jpg. Untuk mendapat UID dapat dengan mudah bisa didapatkan dengan melakukan check stat pada folder air lalu digunakanlah getpwuid dari path yang diisi, lalu untuk mendapat nama dapat dengan mudah dilakukan pw->pw_name. Lalu untuk mendapat UID Permission didapatkan dengan membuat char array dan dicek satu persatu untuk read, write, dan execute dengan S_IRUSR(untuk r), S_IRUSR(untuk w), dan S_IRUSR(untuk x). Lalu untuk tahap terakhir dapat digunakan dengan I/O pada C. Digunakkan open file dengan mode "a" (append) dan untuk menulis per line digunakan fprintf dengan format yang telah diminta. Lalu setelah selesai menggunakan file, akan dilakukan penutupan file untuk mencegah memory leak

```
void addList(int index)
{
  char permission[5];
  struct stat fs;
  int r;
  char path[] = "/home/elelel/modul2/air/";
  r = stat(path, &fs);
  if (r == -1)
  {
    fprintf(stderr, "File error\n");
    exit(1);
  }

  struct passwd *pw = getpwuid(fs.st_uid);
  if (fs.st_mode & S_IRUSR)
    permission[0] = 'r';
  if (fs.st_mode & S_IRUSR)
    permission[1] = 'w';
  if (fs.st_mode & S_IRUSR)
    permission[2] = 'x';

  if (pw != 0)
  {
    FILE *wfile = fopen("/home/elelel/modul2/air/list.txt", "a");
    fprintf(wfile, "%s_%s_%s\n", pw->pw_name, permission, hewanAir[index]);
    fclose(wfile);
  }
}
```

Melakukan pemanggilan function AddList yang telah dijelaskan diatas sebanyak file yang berada dalam folder air pada main.

```
for (int i = 0; i < countAir; i++)
    addList(i);
```

## Output

#### 1. Proses kompilasi file soal3.c

![satu](img/3.1.PNG)

#### 2. Menjalankan program file soal3.exe sehingga menghasilkan folder baru yaitu modul2

![dua](img/3.2.PNG)

#### 3. Hasil dari penjalanan program sebelumnya juga menghasilkan dua folder baru lainnya yaitu darat dan air yang memiliki parent folder modul2

![tiga](img/3.3.PNG)

#### 4. Perbedaan waktu pembuatan antara folder darat dan air yaitu 3s sesuai dengan permintaan soal

![empat](img/3.4.PNG)

#### 5. Isi dari folder darat yang merupakan gambar sesuai dengan ketentuan dari soal yang diminta

![lima](img/3.6.PNG)

#### 6. Isi dari folder air yang merupakan gambar sesuai dengan ketentuan dari soal yang diminta serta sebuah list.txt

![enam](img/3.5.PNG)

#### 7. Isi dari list.txt sesuai dengan yang diminta dari soal yaitu list nama file dalam folder air sesuai dengan format nama yang telah diberikan

![tujuh](img/3.7.PNG)

## Kendala

- Membuat fungsi yang tepat baik dari parameter dan logicnya sehingga tidak membuat code terlalu panjang dan lebih mudah dibaca
